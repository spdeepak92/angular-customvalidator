import { AbstractControl } from '@angular/forms';

export function ValidatePassword(control: AbstractControl) {
  if (control.value.length < 10) {
    return { ValidPassword: true };
  }
  return null;
}