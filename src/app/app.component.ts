import { Component } from '@angular/core';
import { FormGroup, FormControl, Validator } from '@angular/forms';
import { ValidatePassword } from './validators/age.validator';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  loginForm = new FormGroup({
    mobile: new FormControl(''),
    password: new FormControl('', ValidatePassword),
  });

  constructor() { }

  login() {
    console.log(this.loginForm.value);
  }

}